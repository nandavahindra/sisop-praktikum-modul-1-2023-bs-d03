# sisop-praktikum-modul-1-2023-BS-D03

## Nomor 1
**Dalam pengerjaan, file "2023 QS World University Rankings.csv" diubah menjadi "input.csv"**

###### A. Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

```
#Change decimal separator from comma (ID) to dot (US)
export LC_NUMERIC="en_US.UTF-8"
```
Karena dalam file .csv, nilai desimal ditulis menggunakan '.', maka harus diganti setting dari sistem ID menjadi AS.

Karena file .csv sudah diurutkan berdasarkan ranking, maka hanya perlu mencari 5 universitas di Jepang yang pertama kali ditemukan jika dicari dari atas ke bawah.
```
head -n 1 input.csv | tr ',' '|'
```
Pertama, menampilkan header file .csv dengan mengganti tanda ',' dengan '|'
```
awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv | tr ',' ' ' | head -n 5
```
- `awk -F','`, menggunakan awk dengan delimiter ',' untuk membaca setiap kolom sebagai variabel terpisah.
- `'NR>2 {if ($3=="JP") {print $0}}' input.csv`, membaca file input.csv, dimulai dari baris kedua (setelah header) mencetak sebagai output setiap baris dimana kolom ketiga (*location code*) memiliki nilai "JP" (lokasi di Jepang).
- `tr ',' ' ' | head -n 5`, hasil output awk dipipe ke tr dimana tanda koma diganti dengan spasi agar lebih mudah dibaca lalu ditampilkan 5 hasil pertama.


###### Digunakan untuk no 2 dan 3
```
IFS=$'\n' japan=($(awk -F',' 'NR>2 {if ($3=="JP") {print $0}}' input.csv))
```
Berfungsi untuk mencari semua universitas yang berada di Jepang, lalu menyimpannya di dalam array bernama `japan`. 
- `IFS=$'\n'` berarti tanda '\n' digunakan sebagai pemisah antar anggota sehingga setiap baris disimpan dalam index sendiri-sendiri.

###### B. Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 

- `mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n)`
- `printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n`, seluruh isi array `japan` akan dicetak menggunakan printf lalu dipipe ke sort yang akan menyortir berdasarkan : `-t,`(Menganggap ',' sebagai pemisah antar kolom) `-k9,9`(Menyortir menggunakan kolom ke-9 saja) `-n`(Mode menyortir angka).
- `mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k9,9 -n)`, hasil sorting kemudian dipindahkan ke dalam mapfile `japan_sorted` (-t berarti newline setiap baris tidak disimpan).

```
IFS=','

for ((x=0; x<5; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[8]}"
done
```
- `read -a strarr <<< "${japan_sorted[$x]}"`, untuk setiap baris yang tersimpan dalam `japan_sorted`, dipass (<<<) sebagai string dan dibaca ke dalam array `strarr`. `IFS=','` berarti ',' dianggap sebagai pemisah antar kolom.
- `echo "${strarr[1]}|${strarr[8]}"`, mencetak hanya kolom 2 dan 9 (nama universitas dan frs score).

###### C. Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```
echo $'\n'"10 Universities in Japan with highest Employment Outcome Rank (Name, ranking)"
mapfile -t japan_sorted < <(printf "%s\n" "${japan[@]}" | sort -t, -k20,20 -n)
#Untuk setiap baris, menggunakan delimiter ',', sort menggunakan kolom ke 20,20 lalu simpan ke mapfile
IFS=','

for ((x=0; x<10; x++))
do
	read -a strarr <<< "${japan_sorted[$x]}"
	echo "${strarr[1]}|${strarr[19]}"
done
#Menggunakan delimiter ',', baca string pada array ke strarr, lalu baca nama univ, ranking ger
```
Kode mirip dengan poin B, tetapi ada sedikit perbedaan :
- `printf "%s\n" "${japan[@]}" | sort -t, -k20,20 -n`, sorting berdasarkan kolom ke-20 (ranking ger).
- `for ((x=0; x<10; x++))`, mengambil 10 universitas.
- `echo "${strarr[1]}|${strarr[19]}"`, mencetak nama dan rangking ger.

###### D. Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci "keren".
```
echo $'\n''Search result using query = "keren"'

while IFS=',' read -a line
do
	if [[ ${line[1],,} == *"keren"* ]]; then
		echo "${line[1]}"
	fi
done < input.csv
```
- `while IFS=',' read -a line`, while loop berjalan selama masih terdapat line yang bisa dibaca dengan ',' digunakan sebagai pemisah antar kolom.
- `if [[ ${line[1],,} == *"keren"* ]]`, setiap baris dari file .csv dibaca sebagai string dengan semua karakter diubah menjadi lowercase (,,). Apabila terdapat pola "keren", maka cetak.


## Nomor 4

###### log_encrypt.sh
```
curTime=$(date +"%H:%M %d:%m:%Y")
curHour=$(date +"%H")
curHour=$((10#$curHour))

if [ $curHour -lt 1 ]
	then curHour=1
fi
```
Script membaca waktu dan tanggal untuk digunakan sebagai nama file backup (.txt) dan sebagai key caesar cipher yang digunakan.
- `curHour=$((10#$curHour))`, karena jam 1-9 ditulis sebagai 01-09, maka harus dihapus '0' di depannya.
- `if [ $curHour -lt 1 ]`, memeriksa apakah update dilakukan pada jam 00, jika iya tetap m

```
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
num=01234567890123456789
```
Set karakter yang digunakan untuk enkripsi dan dekripsi.
```
offset_num=$(($curHour % 10))
```
Untuk enkripsi angka, diambil hasil modulo 10 sehingga enkripsi jam 6 sama dengan jam 16.
```
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}${num:0:10}" "${az:$curHour:26}${az_cap:$curHour:26}${num:$offset_num:10}" >> "${curTime}.txt"
done < "$syslog_path"
```
- `while IFS= read -r line`, program membaca setiap baris.
- `echo $line | tr "${az:0:26}${az_cap:0:26}${num:0:10}" "${az:$curHour:26}${az_cap:$curHour:26}$`, program membaca baris lalu dipipe ke command `tr`. Menggunakan set karakter yang sudah dideklarasi, setiap karakter digeser maju sebanyak jumlah yang diatur sebelumnya (berdasarkan jam backup).
- `"$syslog_path"`, variabel yang menyimpan absolute path file `syslog`.

###### log_decrypt.sh
Cara kerja decryption serupa dengan encryption karena memanfaatkan sifat looping dari caesar cipher.
- 'a' digeser maju 10 menjadi 'k'
- 'k' digeser maju 16 (26-10) menjadi 'a' karena looping kembali ke awal.

**Script harus disertai 1 argumen, yaitu nama file yang akan didekripsi.**
```
rel_path=./$1
```
Membuat absolute path menggunakan nama file yang akan didekripsi.
```
a=$1
curHour=${a:0:2}
curHour=$((10#$curHour))
curHour=$((26-$curHour))
```
Mengambil nilai jam dari nama file lalu mencari key untuk dekripsi.
```
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
num=01234567890123456789
offset_num=$(($curHour % 10))
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}${num:0:10}" "${az:$curHour:26}${az_cap:$curHour:26}${num:$offset_num:10}" >> "decrypted_$1.txt"
done < "$rel_path"
```
Melakukan proses serupa seperti `log_encrypt.sh` tetapi menerima input file yang akan didekripsi (`$rel_path`).