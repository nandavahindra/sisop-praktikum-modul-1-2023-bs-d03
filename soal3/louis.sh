#!bin/bash

sen=0
user=0
write_log() {
	if [ $sen -lt 1 ]; then
		echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
	else
		echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $user registered successfully" >> log.txt
	fi
}
#------------------------------
echo "Registering new user..."
store="$(dirname "$0")/users/users.txt"

sen=0
while [ $sen -lt 1 ]
do
	read -p "Input username: " user
	if ! grep -qw -m 1 $user "$store"; then
		sen=1
	else
		echo "Username already exists!"
		write_log
	fi
done

sen=0
while [ $sen -lt 1 ]
do
	echo "Valid password requirements:"
	echo -e "\t-Minimum 8 characters"
	echo -e "\t-Minimum 1 uppercase and 1 lowercase letter"
	echo -e "\t-Alphanumeric"
	echo -e "\t-Cannot be the same as username"
	echo -e "\t-Cannot contain the word "chicken" or "ernie""
	read -p "Input valid password: " pass
	if [[ ${#pass} -ge 8 ]]; then
		if [[ $pass =~ [A-Z] ]] && [[ $pass =~ [a-z] ]]; then
			if [[ $pass =~ [^A-Za-z0-9] ]]; then
				echo "ERROR : Must be Alphanumeric"
			else
				if [[ $pass == $user ]] || ([[ $pass =~ chicken ]] || [[ $pass =~ ernie ]]); then
					echo "ERROR : Forbidden word (Username/chicken/ernie)"
				else
					echo "Success"
					sen=1
					write_log
				fi
			fi
		else
			echo "ERROR : Minimum 1 uppercase and 1 lowercase letter"
		fi
	else
		echo "ERROR : Minimum 8 Characters"	
	fi
done

echo $user,$pass >> $store
