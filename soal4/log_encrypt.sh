#! /bin/bash

curTime=$(date +"%H:%M %d:%m:%Y")
curHour=$(date +"%H")
curHour=$((10#$curHour))

syslog_path="/var/log/syslog"
#USED FOR ENCRYPTION AND DECRYPTION
az=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
az_cap=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
while IFS= read -r line; do
	echo $line | tr "${az:0:26}${az_cap:0:26}" "${az:$curHour:26}${az_cap:$curHour:26}" >> "${curTime}.txt"
done < "$syslog_path"

#CRONTAB CONFIG
# * */2 * * * /home/pelangimon/sisop-praktikum-modul-1-2023-bs-d03/soal4/log_encrypt.sh
#CHANGE "pelangimon" with username
